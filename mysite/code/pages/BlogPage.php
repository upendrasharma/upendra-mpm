<?php
class BlogPage extends Page {

	private static $db = array(
		'BlogTitle' => 'Varchar',
		'Date' => 'Date'
	);
	
	private static $has_one = array (
        'BlogImage' => 'Image',
		'Author' => 'Member'
    );
	
		public function getCMSFields() {
			$fields = parent::getCMSFields();
	
			$dateField = new DateField('Date', 'Blog Date');
			$dateField->setConfig('showcalendar', true);
			$dateField->setConfig('dateformat', 'dd/MM/YYYY');
			$fields->addFieldToTab('Root.Main', new TextField('BlogTitle'), 'Content');
			$fields->addFieldToTab('Root.Main', $dateField, 'Content');
			$memberlist = DropdownField::create('MemberID', 'MemberName', Member::get()->map('ID', 'Title'))
                ->setEmptyString('(Select one)');
			$fields->addFieldToTab('Root.Main', $memberlist, 'Content');
			$fields->addFieldToTab('Root.Main', new UploadField('BlogImage'), 'Content');
	
			return $fields;
    	}

}

class BlogPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

}
