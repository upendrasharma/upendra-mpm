<?php
class Page extends SiteTree {

	private static $db = array(
		);

	private static $has_one = array(
		);

}
class Page_Controller extends ContentController {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		);
	
	public function init() {
		parent::init();
		Requirements::combine_files(
			'mpm.js',
			array(
				'themes/mpm/js/jquery-1.11.0.min.js',
				'themes/mpm/js/jquery.custom.js',
				'themes/mpm/js/modernizr.custom.js',
				'themes/mpm/js/jquery.ui.core.min.js',
				'themes/mpm/js/jquery-migrate.min.js',
				'themes/mpm/js/imagesloaded.pkgd.js',
				'themes/mpm/js/masonry.pkgd.min.js',
				'themes/mpm/js/jquery.tools.min.js',
				'themes/mpm/js/jquery.flexslider-min.js',
				'themes/mpm/js/toucheffects.js',
				'themes/mpm/js/classie.js',
				'themes/mpm/js/retina-1.1.0.js',
				'themes/mpm/js/jquery.isotope.js',
				'themes/mpm/js/isotope_init.js',
				'themes/mpm/js/script.js',
				)
			);
	}

}
