<div class="typography">
 
    <% include SideBar %>
 
    <div id="Content">
     
    <% control StaffMember %>
     
        <h2>$Name</h2>
        <p>Role: $Role</p>
         
        $Photo.CroppedImage(250,250)
 
        <p>$Description</p>
     
    <% end_control %>
     
    </div>
 
</div>