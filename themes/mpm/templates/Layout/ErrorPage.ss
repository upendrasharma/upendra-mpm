<!-- page-title-wrap -->
<div id="page-title-wrap">
	<div class="content-skeleton">
		<h1 class="page-title">
			Not Found
		</h1>
		<div id="crumbs">
			<a href="index.html">Home</a>  <span class="current">Not Found</span>
		</div>
	</div><!-- /content-skeleton -->
</div>
<!-- page-title-wrap -->

<!--================== Page Content Starts =======================-->
<section class="content p_top_max">
	<div class="content-skeleton error_page">
		
		
		<div class="error_icon">
			<span class="error-icon-four"></span>
			<span class="error-icon-zero">
				<span class="middle_text">Not Found</span>
			</span>
			<span class="error-icon-four"></span>
			<span class="exclamation-icon"></span>
		</div>
		<div class="error_content">
			<h6>This is somewhat embarrassing, isn't it?</h6>
			<p>It looks like nothing was found at this location. Maybe try a search?</p>
			
			<form method="get" id="searchform">
				<input type="text" placeholder="Search Website..." name="s" id="s" value="Search Website" >
			</form>				
		</div>		
		<div class="clear"></div>			
		
	</div><!-- /.content-skeleton -->
	
	
	<!-- DarkOrange Call to Action Wrap -->
	<div class="call-to-action-wide orange-action-box">
		<div class="content-skeleton">
			<div class="call-to-action-fixed">
				<h3>Ready to take it to the next level?</h3>
				<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</p>
				<a href="" class="button light">Purchase Now</a>
			</div>
		</div><!-- /.content-skeleton -->
	</div>
	<!-- DarkOrange Call to Action Wrap Ends -->
	
	
	
</section><!-- /.content -->
	<!--================== Page Content Ends =======================-->